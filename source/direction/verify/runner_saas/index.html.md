---
layout: markdown_page
title: "Category Direction - Runner SaaS"
description: "Hosted GitLab Runners available on GitLab SaaS and that support the GitLab Build Cloud for Linux, Windows and macOS."
canonical_path: "/direction/verify/runner_cloud/"
---

- TOC
  {:toc}

## Runner SaaS

Runner SaaS is the product development effort that supports runners on GitLab SaaS product offerings. The Runner SaaS product development strategy is an essential component of the GitLab SaaS first strategy. 

## Who we are focusing on?

Check out our [Ops Section Direction "Who's is it for?"](/direction/ops/#who-is-it-for) for an in depth look at the our target personas across Ops. For Runner, our "What's Next & Why" are targeting the following personas, as ranked by priority for support:

1. [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)

##  Runner SaaS Solutions 

| Offer Name | Offer Status|
|----------|----------------|
|Runner SaaS for Linux|GA|
|Runner SaaS for Windows|Beta|
|Runner SaaS for macOS|Beta|

## Strategic Priorities

The table below represents the current strategic priorities for Runner SaaS. This list will change with each monthly revision of this direction page.

|Theme-Category|Item|Why?|Target delivery QTR|
|----------|----------------|----------------|----------------|
|Relilability|[Continuous Delivery: Runner SaaS for Linux](https://gitlab.com/groups/gitlab-org/-/epics/1453)|Improving the deployment process for the Linux Runners on GitLab SaaS is a key strategic initiative as it enables us to roll out updates to the environment more quickly. Shorter deployment times will improve our ability to add platform capacity or rollout code changes to resolve user or customer-impacting issues that may arise on GitLab CI SaaS.|FY22 Q4|
|Scalability|[Segmentation by plan type](https://gitlab.com/gitlab-org/gitlab/-/issues/323525#)|Today we do not delineate access to tiers of service on Runner SaaS by plan type. As we continue to grow GitLab SaaS, a critical next phase is to offer different Runner SaaS capabilities and service levels by plan type. This foundational capability not only sets the stage for those future offers, but is also another pillar in enabling us to consistently deliver service levels in line with our stated SLO targets.|FY22 Q4|
|macOS|[Runner Cloud for macOS General Availability](https://gitlab.com/groups/gitlab-org/-/epics/6105)|In Q3 FY22, we launched the Runner Cloud for macOS beta. To date, the user and customer demand for this service has been exceptional. And further reinforcing the one DevOps strategy, the current beta participants strongly favor having a fully integrated build solution for the Apple ecosystem embedded in GitLab. We plan to launch this offer in GA and to evolve it so that customers on GitLab SaaS can simply focus on writing code  and be rest assured that the capabilities to build their software is fully managed by GitLab|FY23 Q1|
|Scalability|[Implement a new autoscaling solution - Runner SaaS for Linux OS](https://gitlab.com/groups/gitlab-org/-/epics/6995)|The Runner SaaS for Linux handles millions of CI jobs each month. The autoscaling architecture that is the foundation of this solution has served us well. However, since its introduction, we have launched new autoscaling solutions to manage the Runner Cloud for Windows and macOS. In addition, the core technology used for Linux, Docker Machine, is no longer maintained by Docker. Though we maintain a fork of Docker Machine, the right long-term strategy is to iterate towards the next generation autoscaler for the Runner SaaS. This architectural technology pivot will enable us to accomplish several goals. First - enabling GitLab to scale over the next five years. And in the near term, ensure that we can consistently achieve and exceed our CI Runners SLO goal of 99.95% availability.|FY23 Q2|
|Windows|[Runner SaaS for Windows General Availability](https://gitlab.com/groups/gitlab-org/-/epics/6631)|We launched the Runner SaaS for Windows in beta in FY21Q1. At the time, our initial plan was to transition the offer to GA within a few quarters. However, due to other strategic priorities, we delayed that timeline. Now users and customers rely on a Windows build solution hosted on GitLab SaaS for their mission-critical CI/CD builds, so we must migrate the Windows offer to GA.|FY23 Q3|

## Ongoing Maintenance

In conjunction with the development work required to deliver the strategic priorities listed above, in each milestone, the Runner Cloud team will devote up to 40% of available developer capacity across the categories listed below.

- macOS build VM image updates and maintenance.
- Windows build VM image updates and maintenance.
- Bugs related to executing builds on Runner Cloud (Linux + Docker, macOS, Windows).
- Collaboration with the SRE team to resolve incidents related to Runner Cloud..
- Installing new runner version updates to the runner managers on Runner Cloud.

## Maturity Plan

The Runner SaaS maturity level is ["Minimal"](/direction/maturity/). We are also evaluating this category with a Category maturity scorecard via [gitlab&6090](https://gitlab.com/groups/gitlab-org/-/epics/6090). For more information (see our [definitions of maturity levels](/direction/maturity/)).

## Competitive Landscape

Organizations that use Cloud-native CI/CD solutions, such as GitLab.com, CircleCI, and GitHub, can run their CI/CD pipelines and get to a first green build without setting up build servers, installing and configuring build agents, or runners.

In addition to eliminating CI build server maintenance costs, there are other critical considerations for organizations that can migrate 100% of their CI/CD processes to a cloud-native solution. These include security, reliability, performance, multiple build server and configuration options, and on-demand scale.

## Competitive Matrix

|Solution|Build server environments offered|Build server options|
|----------|----------------|----------------|
|Runner Cloud|Linux, Windows, macOS|Linux = 1 vCPU, 3.75 GB RAM; Windows = 2 vCPU, 7.5 GB| 
|GitHub Actions|Linux, Windows, macOS|Linux & Windows = Microsoft Azure  Standard_DS2_v2 VM's ( 2 vCPU, 7 GB RAM; macOS = 3 vCPU, 14 GB RAM|
|CircleCI|Linux, Windows, macOS| CircleCI machine executor = 2 vCPU, 7.5 GB RAM, 4 vCPU, 15 GB RAM, 8 vCPU, 32 GB RAM, 16 vCPU, 64 GB RAM;|

## Give Feedback

If you have questions about a specific runner feature request or have a requirement that's not yet in our backlog, you can provide feedback or open an issue in the GitLab Runner [repository](https://gitlab.com/gitlab-org/gitlab-runner/-/issues).

## Revision Date

This direction page was revised on: 2021-11-02
